Format: 1.0
Source: pari-extra
Binary: pari-extra
Architecture: all
Version: 3-1
Maintainer: Bill Allombert <ballombe@debian.org>
Standards-Version: 3.9.2.0
Build-Depends: debhelper (>= 5)
Package-List:
 pari-extra deb math optional arch=all
Checksums-Sha1:
 ff281e103ab11681324b0c694dd3514d78436c51 121 pari-extra_3.orig.tar.gz
 ca13e48c1b8e063bd33a8c897c44b2fa54c7b607 2357 pari-extra_3-1.diff.gz
Checksums-Sha256:
 ac1ef39f9da80b582d1c0b2adfb09b041e3860ed20ddcf57a0e922e3305239df 121 pari-extra_3.orig.tar.gz
 90b5f4bdda25d1bf39530cc14310f51d88c8696eef2589f4a7f5991596fe7b1d 2357 pari-extra_3-1.diff.gz
Files:
 76bcf03be979d3331f9051aa88439b8b 121 pari-extra_3.orig.tar.gz
 264a508299ea6d57c6a386e26d9d6f49 2357 pari-extra_3-1.diff.gz
